/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef SM_VALUE_H_
#define SM_VALUE_H_

#include <smite/defs.h>

#include <stdbool.h>
#include <stdint.h>

#ifdef SM_64
typedef uint64_t sm_word;
typedef uint64_t sm_bits;

#define SM_IMMEDIATE_MARK_BITS 0x00000003
#define SM_IMMEDIATE_TYPE_BITS 0x0000000f

#define SM_BOOL_BITS    0x00000006
#define SM_CHAR_BITS    0x0000000a
#define SM_SPECIAL_BITS 0x0000000e

#define SM_FALSE ((sm_word)(SM_BOOL_BITS | 0x00000000))
#define SM_TRUE  ((sm_word)(SM_BOOL_BITS | 0x00000010))

#define SM_NIL  ((sm_word)(SM_SPECIAL_BITS | 0x00000000))
#define SM_VOID ((sm_word)(SM_SPECIAL_BITS | 0x00000010))
#define SM_EOF  ((sm_word)(SM_SPECIAL_BITS | 0x00000030))

#define SM_FIXNUM_BIT   0x00000001
#define SM_FIXNUM_SHIFT 1

#define SM_FIX(n)   (((sm_word)(n) << SM_FIXNUM_SHIFT) | SM_FIXNUM_BIT)
#define SM_UNFIX(v) ((v) >> SM_FIXNUM_SHIFT)

/* Character range is that of a UTF-8 codepoint, not representable range */
#define SM_CHAR_BIT_MASK 0x1fffff
#define SM_CHAR_SHIFT    8

#define SM_IS_OBJ(val) (!(val & SM_IMMEDIATE_MARK_BITS))

#define SM_IS_VOID(val)   (val == SM_VOID)
#define SM_IS_NIL(val)    (val == SM_NIL)
#define SM_IS_EOF(val)    (val == SM_EOF)
#define SM_IS_FALSE(val)  (val == SM_FALSE)
#define SM_IS_TRUE(val)   (val == SM_TRUE)
#define SM_IS_BOOL(val)   (SM_IS_FALSE(val) || SM_IS_TRUE(val))
#define SM_IS_FIXNUM(val) (val & SM_FIXNUM_BIT)
#else
#error not 64-bit
#endif

#endif
