/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef SM_DEFS_H_
#define SM_DEFS_H_

#define FLEXIBLE_ARRAY 0

#define SM_DEFAULT_AHEAP_SIZE 65536

#if defined(__clang__)
#define SM_COMPILER_CLANG 1
#elif defined(__INTEL_COMPILER)
#define SM_COMPILER_ICC 1
#elif defined(__GNUC__) || defined(__GNUG__)
#define SM_COMPILER_GCC 1
#elif defined(_MSC_VER)
#define SM_COMPILER_MSVC 1
#else
#warning "Compiler not detected"
#endif

/* detect endianess */

#if defined(SM_COMPILER_MSVC)
#define SM_LITTLE_ENDIAN
#elif defined(__BYTE_ORDER__) && __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#define SM_LITTLE_ENDIAN
#elif defined(__BYTE_ORDER__) && __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#define SM_BIG_ENDIAN
#else
#error "Byte order not detected"
#endif

#if defined(SM_COMPILER_GCC) || defined(SM_COMPILER_CLANG) ||                  \
    defined(SM_COMPILER_ICC)
#define SM_UNLIKELY(x) __builtin_expect(!!(x), 0)
#define SM_LIKELY(x)   __builtin_expect(!!(x), 1)
#else
#define SM_UNLIKELY(x) (x)
#define SM_LIKELY(x)   (x)
#endif

#ifndef SM_64
#if defined(__alpha__) || defined(__sparc_v9__) || defined(__sparcv9) ||       \
    defined(__ia64__) || defined(__x86_64__) || defined(__LP64__) ||           \
    defined(__powerpc64__)
#define SM_64
#elif defined(__mips64) && (!defined(__GNUC__) || _MIPS_SZPTR == 64)
#define SM_64
#endif
#endif

#endif
