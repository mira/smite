/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef SMITE_H_
#define SMITE_H_

typedef struct _sm_state sm_state;

#include <smite/obj.h>
#include <stdio.h>

sm_state *sm_make_state(void);
void sm_free_state(sm_state *state);
int sm_allocated_words(sm_state *state);
int sm_allocated_bytes(sm_state *state);
sm_word sm_intern(sm_state *state, sm_word ns, sm_word name);
sm_word sm_intern_c(sm_state *state, const char *ns, const char *name);
sm_word sm_symbol_table(sm_state *state);
sm_word sm_get_ns(sm_state *state, sm_word name);
sm_word sm_ns_table(sm_state *state);
sm_word sm_var(sm_state *state, sm_word symbol);
sm_word sm_def(sm_state *state, sm_word symbol, sm_word val);
sm_word sm_current_ns(sm_state *state);

typedef struct _sm_reader {
	enum {
		FILE_READER,
		STRING_READER,
	} type;
	int c;
	union {
		struct {
			const char *data;
			int size;
		} str;
		FILE *file;
	};
} sm_reader;

void sm_file_reader(sm_state *state, FILE *f, sm_reader *reader);
void sm_string_reader(sm_state *state, const char *str, int size, sm_reader *reader);
int sm_read(sm_state *state, sm_reader *reader, sm_word *dst);

int sm_load_file(sm_state *state, const char *name, sm_word *dst);
int sm_load_string(sm_state *state, const char *str, sm_word *dst);

int sm_eval(sm_state *state, sm_word expr, sm_word ns, sm_word *dst);
int sm_compile(sm_state *state, sm_word expr, sm_word ns, sm_word *dst);

int sm_load_motmot(sm_state *state);

void sm_print_value(sm_word val);

#endif
