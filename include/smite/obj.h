/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef SM_OBJ_H_
#define SM_OBJ_H_

#include <smite/value.h>

typedef int (*sm_ff_t)(sm_state *state, sm_word *args, int n_args);

#define SM_OFFSET(obj, offset)     *((sm_word *)(obj) + (offset))
#define SM_RAW_OFFSET(obj, offset) *((uint8_t *)((sm_word *)obj + 1) + offset)
#define SM_RAW(obj)                ((uint8_t *)((sm_word *)obj + 1))

#define SM_INT_SIGN_BIT       0x8000000000000000L
#define SM_INT_TOP_BIT        0x4000000000000000L
#define SM_HEADER_BITS_MASK   0xff00000000000000L
#define SM_HEADER_TYPE_MASK   0x0f00000000000000L
#define SM_HEADER_SIZE_MASK   0x00ffffffffffffffL
#define SM_GC_FORWARDING_FLAG 0x8000000000000000L
#define SM_RAW_FLAG           0x4000000000000000L
#define SM_SPECIAL_FLAG       0x2000000000000000L
#define SM_FROZEN_FLAG        0x1000000000000000L
#define SM_OBJ_TYPE(obj)      (SM_OFFSET(obj, 0) & SM_HEADER_BITS_MASK)
#define SM_OBJ_SIZE(obj)      ((SM_OFFSET(obj, 0)) & SM_HEADER_SIZE_MASK)

/* CAR, CDR */
#define SM_PAIR_TYPE    0x0100000000000000L
/* ... */
#define SM_TUPLE_TYPE   0x0200000000000000L
/* ... */
#define SM_BINARY_TYPE  (0x0200000000000000L | SM_RAW_FLAG)
/* MIN_CAP, USED, ENTRIES */
#define SM_TABLE_TYPE   0x0300000000000000L
/* NS, NAME */
#define SM_SYMBOL_TYPE  0x0400000000000000L
/* NAME, VALUE */
#define SM_VAR_TYPE     0x0500000000000000L
/* NAME, ALIAS, MAPPINGS */
#define SM_NS_TYPE      0x0600000000000000L
/* PROTO, UPVALS ... */
#define SM_CLOSURE_TYPE 0x0700000000000000L
#define SM_FF_TYPE      (0x0700000000000000L | SM_SPECIAL_FLAG)
/* NAME, CODE, CONSTS ... */
#define SM_PROTO_TYPE   0x0800000000000000L
/* CLOSURE */
#define SM_MACRO_TYPE   0x0900000000000000L

#define SM_PAIR_SIZE            2
#define SM_SYMBOL_SIZE          2
#define SM_BINARY_SIZE(size)    ((size + sizeof(sm_word) - 1) / sizeof(sm_word))
#define SM_TABLE_SIZE           3
#define SM_SYMBOL_SIZE          2
#define SM_VAR_SIZE             2
#define SM_NS_SIZE              3
#define SM_CLOSURE_SIZE(upvals) (1 + (upvals))
#define SM_FF_SIZE(upvals)      (1 + (upvals))
#define SM_PROTO_SIZE(consts)   (2 + (consts))
#define SM_MACRO_SIZE           1

/* type predicates */
#define SM_IS_OBJ_TYPE(val, type)                                              \
	(SM_IS_OBJ(val) && ((SM_OFFSET(val, 0) & SM_HEADER_BITS_MASK) == type))
#define SM_IS_PAIR(val)    (SM_IS_OBJ_TYPE(val, SM_PAIR_TYPE))
#define SM_IS_TUPLE(val)   (SM_IS_OBJ_TYPE(val, SM_TUPLE_TYPE))
#define SM_IS_BINARY(val)  (SM_IS_OBJ_TYPE(val, SM_BINARY_TYPE))
#define SM_IS_TABLE(val)   (SM_IS_OBJ_TYPE(val, SM_TABLE_TYPE))
#define SM_IS_SYMBOL(val)  (SM_IS_OBJ_TYPE(val, SM_SYMBOL_TYPE))
#define SM_IS_VAR(val)     (SM_IS_OBJ_TYPE(val, SM_VAR_TYPE))
#define SM_IS_NS(val)      (SM_IS_OBJ_TYPE(val, SM_NS_TYPE))
#define SM_IS_CLOSURE(val) (SM_IS_OBJ_TYPE(val, SM_CLOSURE_TYPE))
#define SM_IS_PROTO(val)   (SM_IS_OBJ_TYPE(val, SM_PROTO_TYPE))
#define SM_IS_MACRO(val)   (SM_IS_OBJ_TYPE(val, SM_MACRO_TYPE))

#define SM_IS_LIST(val) (SM_IS_NIL(val) || SM_IS_PAIR(val))

#define SM_CAR(val) SM_OFFSET(val, 1)
#define SM_CDR(val) SM_OFFSET(val, 2)

#define SM_TABLE_MIN_CAP(val)  SM_OFFSET(val, 1)
#define SM_TABLE_USED(val)     SM_OFFSET(val, 2)
#define SM_TABLE_ENTRIES(val)  SM_OFFSET(val, 3)
#define SM_TABLE_CAP(val)      SM_OBJ_SIZE(SM_TABLE_ENTRIES(table))
#define SM_TABLE_ENTRY(val, i) SM_OFFSET(SM_TABLE_ENTRIES(val), i + 1)

#define SM_SYMBOL_NS(val)   SM_OFFSET(val, 1)
#define SM_SYMBOL_NAME(val) SM_OFFSET(val, 2)

#define SM_VAR_NAME(val)  SM_OFFSET(val, 1)
#define SM_VAR_VALUE(val) SM_OFFSET(val, 2)

#define SM_NS_NAME(val)     SM_OFFSET(val, 1)
#define SM_NS_ALIAS(val)    SM_OFFSET(val, 2)
#define SM_NS_MAPPINGS(val) SM_OFFSET(val, 3)

#define SM_CLOSURE_PROTO(val)    SM_OFFSET(val, 1)
#define SM_CLOSURE_UPVAL(val, i) SM_OFFSET(val, 2 + i)

#define SM_FF_PROTO(val)    ((sm_ff_t)SM_OFFSET(val, 1))
#define SM_FF_UPVAL(val, i) SM_OFFSET(val, 2 + i)

#define SM_PROTO_NAME(val)     SM_OFFSET(val, 1)
#define SM_PROTO_CODE(val)     SM_OFFSET(val, 2)
#define SM_PROTO_CONST(val, i) SM_OFFSET(val, 3 + i)

#define SM_MACRO_CLOSURE(val) SM_OFFSET(val, 1)

sm_word sm_make_pair(sm_state *state, sm_word car, sm_word cdr);
sm_word sm_make_tuple(sm_state *state, sm_word *data, sm_bits size);
sm_word sm_make_empty_tuple(sm_state *state, sm_bits size);
sm_word sm_make_binary(sm_state *state, const char *data, sm_bits size);
sm_word sm_make_binary_p(sm_state *state, const char *data, sm_bits size);
sm_word sm_make_empty_table(sm_state *state, sm_bits size);

uint32_t sm_hash_binary(sm_word binary);

void sm_table_set(sm_state *state, sm_word table, sm_word key, sm_word val);
sm_word sm_table_get(sm_state *state, sm_word table, sm_word key);

sm_word sm_make_symbol(sm_state *state, sm_word ns, sm_word name);
sm_word sm_make_var(sm_state *state, sm_word name);
sm_word sm_make_ns(sm_state *state, sm_word name);
void sm_ns_alias(sm_state *state, sm_word ns, sm_word t, sm_word alias);
void sm_ns_map(sm_state *state, sm_word ns, sm_word name, sm_word var);
bool sm_symbol_has_ns(sm_word symbol);

sm_word sm_make_proto(sm_state *state, sm_word name, sm_word code,
                      sm_word *consts, int n_consts);

sm_word sm_make_closure(sm_state *state, sm_word proto, sm_word *upvals,
                        int n_upvals);
sm_word sm_make_ff(sm_state *state, sm_ff_t fn, sm_word *upvals, int n_upvals);
sm_word sm_make_macro(sm_state *state, sm_word closure);

sm_word sm_make_list(sm_state *state, sm_word *data, sm_bits size);

int sm_is_eq(sm_word a, sm_word b);
sm_word sm_eq_p(sm_word a, sm_word b);

#endif
