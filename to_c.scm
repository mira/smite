(use-modules (ice-9 string-fun) (ice-9 rdelim))

(define out (open-output-file (cadr (command-line))))
(define in (open-input-file (caddr (command-line))))

(define *PREAMBLE* "/* Generated automatically. Do not edit. */\nstatic const char *lib_src =\n")

(display *PREAMBLE* out)

(let loop ()
  (let ((line (read-line in)))
    (when (not (eof-object? line))
      (set! line (string-replace-substring line "\"" "\\\""))
      (display "\"" out)
      (display line out)
      (display "\"\n" out)
      (loop))))

(display ";\n" out)
