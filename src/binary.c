/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "state.h"

sm_word sm_make_binary(sm_state *state, const char *data, sm_word size)
{
	sm_word binary = sm_aalloc(state, SM_BINARY_SIZE(size) + 1);
	SM_OFFSET(binary, 0) = SM_BINARY_TYPE | size;
	memcpy(SM_RAW(binary), data, size);
	return binary;
}

sm_word sm_make_binary_p(sm_state *state, const char *data, sm_bits size)
{
	sm_word binary = sm_palloc(state, SM_BINARY_SIZE(size) + 1);
	SM_OFFSET(binary, 0) = SM_BINARY_TYPE | size;
	memcpy(SM_RAW(binary), data, size);
	return binary;
}

uint32_t sm_hash_binary(sm_word binary)
{
	if (!SM_IS_BINARY(binary)) {
		return 0;
	}
	uint8_t *data = SM_RAW(binary);
	sm_word len = SM_OBJ_SIZE(binary);
	/* FNV-1a hash. See: http://www.isthe.com/chongo/tech/comp/fnv/ */
	uint32_t hash = 2166136261u;
	for (uint32_t i = 0; i < len; i++) {
		hash ^= data[i];
		hash *= 16777619;
	}
	return hash;
}
