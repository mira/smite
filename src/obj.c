/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "state.h"

sm_word sm_make_pair(sm_state *state, sm_word car, sm_word cdr)
{
	sm_word pair = sm_aalloc(state, SM_PAIR_SIZE + 1);
	SM_OFFSET(pair, 0) = SM_PAIR_TYPE | SM_PAIR_SIZE;
	SM_OFFSET(pair, 1) = car;
	SM_OFFSET(pair, 2) = cdr;
	return pair;
}

sm_word sm_make_tuple(sm_state *state, sm_word *data, sm_word size)
{
	sm_word tuple = sm_aalloc(state, size + 1);
	SM_OFFSET(tuple, 0) = SM_TUPLE_TYPE | size;
	for (int i = 0; i < size; i++) {
		SM_OFFSET(tuple, i + 1) = data[i];
	}
	return tuple;
}

sm_word sm_make_empty_tuple(sm_state *state, sm_word size)
{
	sm_word tuple = sm_aalloc(state, size + 1);
	SM_OFFSET(tuple, 0) = SM_TUPLE_TYPE | size;
	for (int i = 0; i < size; i++) {
		SM_OFFSET(tuple, i + 1) = SM_VOID;
	}
	return tuple;
}

sm_word sm_make_var(sm_state *state, sm_word name)
{
	sm_word var = sm_aalloc(state, SM_VAR_SIZE + 1);
	SM_OFFSET(var, 0) = SM_VAR_TYPE | SM_VAR_SIZE;
	SM_OFFSET(var, 1) = name;
	SM_OFFSET(var, 2) = SM_VOID;
	return var;
}

sm_word sm_make_proto(sm_state *state, sm_word name, sm_word code,
                      sm_word *consts, int n_consts)
{
	sm_word proto = sm_aalloc(state, SM_PROTO_SIZE(n_consts) + 1);
	SM_OFFSET(proto, 0) = SM_PROTO_TYPE | SM_PROTO_SIZE(n_consts);
	SM_OFFSET(proto, 1) = name;
	SM_OFFSET(proto, 2) = code;
	for (int i = 0; i < n_consts; i++) {
		SM_OFFSET(proto, 3 + i) = consts[i];
	}
	return proto;
}

sm_word sm_make_closure(sm_state *state, sm_word proto, sm_word *upvals,
                        int n_upvals)
{
	sm_word closure = sm_aalloc(state, SM_CLOSURE_SIZE(n_upvals) + 1);
	SM_OFFSET(closure, 0) = SM_CLOSURE_TYPE | SM_CLOSURE_SIZE(n_upvals);
	SM_OFFSET(closure, 1) = proto;
	for (int i = 0; i < n_upvals; i++) {
		SM_CLOSURE_UPVAL(closure, i) = upvals[i];
	}
	return closure;
}

sm_word sm_make_ff(sm_state *state, sm_ff_t fn, sm_word *upvals, int n_upvals)
{
	sm_word ff = sm_aalloc(state, SM_FF_SIZE(n_upvals) + 1);
	SM_OFFSET(ff, 0) = SM_FF_TYPE | SM_FF_SIZE(n_upvals);
	SM_OFFSET(ff, 1) = (sm_word)fn;
	for (int i = 0; i < n_upvals; i++) {
		SM_FF_UPVAL(ff, i) = upvals[i];
	}
	return ff;
}

sm_word sm_make_macro(sm_state *state, sm_word closure)
{
	sm_word macro = sm_aalloc(state, SM_MACRO_SIZE + 1);
	SM_OFFSET(macro, 0) = SM_MACRO_TYPE | SM_MACRO_SIZE;
	SM_OFFSET(macro, 1) = closure;
	return macro;
}

int sm_is_eq(sm_word a, sm_word b)
{
	return a == b;
}

sm_word sm_eq_p(sm_word a, sm_word b)
{
	if (sm_is_eq(a, b)) {
		return SM_TRUE;
	} else {
		return SM_FALSE;
	}
}

int sm_ref_fun(sm_state *state, sm_word *args, int n_args)
{
	if (n_args != 1) {
		return -1;
	} else if (!SM_IS_FIXNUM(args[1])) {
		return -1;
	} else if (!SM_IS_OBJ(args[0])) {
		return -1;
	}
	int offset = SM_UNFIX(args[1]);
	if (offset >= SM_OBJ_SIZE(args[0])) {
		return -1;
	}
	switch (SM_OBJ_TYPE(args[0])) {
	case SM_BINARY_TYPE:
		args[0] = SM_FIX(SM_RAW_OFFSET(args[0], offset));
		return 0;
	case SM_TUPLE_TYPE:
		args[0] = SM_OFFSET(args[0], offset + 1);
		return 0;
	default:
		return -1;
	}
	
}
