/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "common.h"

void sm_print_value(sm_word val);

static void print_pair(sm_word obj)
{
	printf("(");
	sm_print_value(SM_CAR(obj));
	obj = SM_CDR(obj);
	while (SM_IS_PAIR(obj)) {
		printf(" ");
		sm_print_value(SM_CAR(obj));
		obj = SM_CDR(obj);
	}
	if (!SM_IS_NIL(obj)) {
		printf(" . ");
		sm_print_value(obj);
	}
	printf(")");
}

static void print_symbol(sm_word obj)
{
	if (sm_symbol_has_ns(obj)) {
		sm_word ns = SM_SYMBOL_NS(obj);
		sm_word name = SM_SYMBOL_NAME(obj);
		printf("%.*s.%.*s", (int)SM_OBJ_SIZE(ns), SM_RAW(ns),
		       (int)SM_OBJ_SIZE(name), SM_RAW(name));
	} else {
		sm_word name = SM_SYMBOL_NAME(obj);
		printf("%.*s", (int)SM_OBJ_SIZE(name), SM_RAW(name));
	}
}

static void print_var(sm_word obj)
{
	sm_word name = SM_VAR_NAME(obj);
	if (SM_IS_NIL(name)) {
		printf("#<var>");
	} else {
		printf("#<var ");
		print_symbol(name);
		printf(">");
	}
}

static void print_obj(sm_word obj)
{
	switch (SM_OBJ_TYPE(obj)) {
	case SM_BINARY_TYPE:
		printf("%.*s", (int)SM_OBJ_SIZE(obj), SM_RAW(obj));
		break;
	case SM_PAIR_TYPE:
		print_pair(obj);
		break;
	case SM_SYMBOL_TYPE:
		print_symbol(obj);
		break;
	case SM_VAR_TYPE:
		print_var(obj);
		break;
	case SM_FF_TYPE:
	case SM_CLOSURE_TYPE:
		printf("#<proc>");
		break;
	}
}

void sm_print_value(sm_word val)
{
	if (SM_IS_OBJ(val)) {
		print_obj(val);
	} else if (SM_IS_FIXNUM(val)) {
		printf("%li", SM_UNFIX(val));
	} else if (SM_IS_NIL(val)) {
		printf("()");
	}
}
