/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "state.h"

sm_word sm_make_symbol(sm_state *state, sm_word ns, sm_word name)
{
	sm_word symbol = sm_aalloc(state, SM_SYMBOL_SIZE + 1);
	SM_OFFSET(symbol, 0) = SM_SYMBOL_TYPE | SM_SYMBOL_SIZE;
	SM_OFFSET(symbol, 1) = ns;
	SM_OFFSET(symbol, 2) = name;
	return symbol;
}

static int raw_symbol_cmp(sm_word symbol, const char *ns, size_t ns_size,
                          const char *name, size_t name_size)
{
	if (!SM_IS_NIL(SM_SYMBOL_NS(symbol))) {
		if (SM_OBJ_SIZE(SM_SYMBOL_NS(symbol)) != ns_size) {
			return 0;
		}
		if (strncmp((char *)SM_RAW(SM_SYMBOL_NS(symbol)), ns,
		            ns_size)) {
			return 0;
		}
	}
	if (SM_OBJ_SIZE(SM_SYMBOL_NAME(symbol)) != name_size) {
		return 0;
	}
	if (strncmp((char *)SM_RAW(SM_SYMBOL_NAME(symbol)), name, name_size)) {
		return 0;
	}
	return 1;
}

static sm_word make_symbol(sm_state *state, const char *ns, sm_bits ns_size,
                           const char *name, sm_bits name_size)
{
	if (ns != NULL) {
		return sm_make_symbol(state, sm_make_binary(state, ns, ns_size),
		                      sm_make_binary(state, name, name_size));
	} else {
		return sm_make_symbol(state, SM_NIL,
		                      sm_make_binary(state, name, name_size));
	}
}

/* TODO: clean this up */
static sm_word intern_symbol(sm_state *state, const char *ns, sm_bits ns_size,
                             const char *name, sm_bits name_size)
{
	sm_word table = state->symbols;
	sm_word size = SM_TABLE_CAP(table);
	if (size == 0) {
		return -1;
	} else if (SM_TABLE_USED(table) + 1 >
	           SM_TABLE_CAP(table) * SM_TABLE_MAX_LOAD / 100) {
		sm_word cap = SM_TABLE_CAP(table) * SM_TABLE_GROW_FACTOR;
		sm_resize_table(state, table, cap);
	}

	uint32_t hash = sm_hash_symbol_raw(ns, ns_size, name, name_size);
	uint32_t start_i = hash % size;
	uint32_t i = start_i;

	sm_word empty = SM_NIL;
	do {
		sm_word entry = SM_TABLE_ENTRY(table, i);
		if (SM_IS_VOID(entry)) {
			/* key is not in table */
			/* create a new key */
			sm_word symbol =
			    make_symbol(state, ns, ns_size, name, name_size);
			sm_word pair = sm_make_pair(state, symbol, SM_VOID);
			SM_TABLE_ENTRY(table, i) = pair;
			SM_TABLE_USED(table) =
			    SM_FIX(SM_UNFIX(SM_TABLE_USED(table)) + 1);
			return symbol;
		} else if (SM_IS_VOID(SM_CAR(entry))) {
			if (SM_IS_NIL(empty)) {
				empty = entry;
			}
		} else if (raw_symbol_cmp(SM_CAR(entry), ns, ns_size, name,
		                          name_size)) {
			return SM_CAR(entry);
		}
		i = (i + 1) % size;
	} while (i != start_i);
	/* either the table is full or there's an empty slot */
	if (empty == SM_NIL) {
		/* table is full */
		return SM_NIL;
	} else {
		sm_word symbol =
		    make_symbol(state, ns, ns_size, name, name_size);
		SM_CAR(empty) = symbol;
		SM_CDR(empty) = SM_VOID;
		SM_TABLE_USED(table) =
		    SM_FIX(SM_UNFIX(SM_TABLE_USED(table)) + 1);
		return symbol;
	}
}

sm_word sm_intern(sm_state *state, sm_word ns, sm_word name)
{
	if (SM_IS_NIL(ns)) {
		return intern_symbol(state, NULL, 0, (char *)SM_RAW(name),
		                     SM_OBJ_SIZE(name));
	} else {
		return intern_symbol(state, (char *)SM_RAW(ns), SM_OBJ_SIZE(ns),
		                     (char *)SM_RAW(name), SM_OBJ_SIZE(name));
	}
}

sm_word sm_intern_c(sm_state *state, const char *ns, const char *name)
{
	if (ns == NULL) {
		return intern_symbol(state, NULL, 0, name, strlen(name));
	} else {
		return intern_symbol(state, ns, strlen(ns), name, strlen(name));
	}
}

bool sm_symbol_has_ns(sm_word symbol)
{
	return !SM_IS_NIL(SM_SYMBOL_NS(symbol));
}
