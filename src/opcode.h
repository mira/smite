/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef SM_OPCODE_H_
#define SM_OPCODE_H_
#define DEFINE_OPS(_)                                                          \
	_(NOP, "nop", 0)                                                       \
	_(LD_CONST, "ld-const", 1)                                             \
	_(POP, "pop", -1)                                                      \
	_(VAR_DEREF, "var-deref", 0)                                           \
	_(VAR_SET, "var-set", -1)                                              \
	_(CALL_N, "call-n", 0)                                                 \
	_(STORE_LOCAL, "store-local", -1)                                      \
	_(LOAD_LOCAL, "load-local", 1)                                         \
	_(CLOSURE, "closure", 1)                                               \
	_(BRANCH, "branch", -1)                                                \
	_(JUMP, "jump", 0)                                                     \
	_(BIND_REST, "bind-rest", 0)                                           \
	_(RET, "ret", 0)

#define OPDEF(OP, NAME, EFFECT) OP_##OP,
enum {
	DEFINE_OPS(OPDEF) OP_UNUSED,
};
#undef OPDEF

extern const int sm_op_effect[];

#endif
