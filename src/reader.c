/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "common.h"
#include "state.h"
#include <ctype.h>
#include <stdio.h>

static int read(sm_state *state, sm_reader *reader, sm_word *dst);

static int next(sm_reader *reader)
{
	if (reader->type == FILE_READER) {
		reader->c = getc(reader->file);
	} else if (reader->type == STRING_READER) {
		if (reader->str.size == 0) {
			reader->c = EOF;
			return EOF;
		}
		reader->c = *reader->str.data++;
		reader->str.size--;
	}
	return reader->c;
}

static int back(sm_reader *reader)
{
	if (reader->type == FILE_READER) {
		reader->c = ungetc(reader->c, reader->file);
	} else if (reader->type == STRING_READER) {
		reader->c = *reader->str.data--;
		reader->str.size++;
	}
	return reader->c;
}

static char is_delimiter(int c)
{
	return isspace(c) || c == EOF || c == '(' || c == ')' || c == '"' ||
	       c == ';';
}

static char is_initial(int c)
{
	return isalpha(c) || c == '*' || c == '/' || c == '>' || c == '<' ||
	       c == '=' || c == '?' || c == '!' || c == '+' || c == '-' ||
	       c == '.';
}

static int peek(sm_reader *reader)
{
	int c;

	c = next(reader);
	back(reader);
	return c;
}

static void skip_whitespace(sm_reader *reader)
{
	int c;
	while ((c = next(reader)) != EOF) {
		if (isspace(c)) {
			continue;
		} else if (c == ';') {
			while (((c = next(reader)) != EOF) && (c != '\n'))
				;
			continue;
		}
		back(reader);
		break;
	}
}

static int read_pair(sm_state *state, sm_reader *reader, sm_word *dst)
{
	int c;
	sm_word car;
	sm_word cdr;

	skip_whitespace(reader);

	c = next(reader);
	if (c == ')') {
		*dst = SM_NIL;
		return 0;
	}

	back(reader);

	if (read(state, reader, &car) != 0) {
		return -1;
	}

	skip_whitespace(reader);

	c = next(reader);
	if (c == '.') {
		c = peek(reader);
		if (!is_delimiter(c)) {
			return -1;
		}
		if (read(state, reader, &cdr) != 0) {
			return -1;
		}
		skip_whitespace(reader);
		c = next(reader);
		if (c != ')') {
			return -1;
		}
		*dst = sm_make_pair(state, car, cdr);
		return 0;
	} else {
		back(reader);
		if (read_pair(state, reader, &cdr) != 0) {
			return -1;
		}
		*dst = sm_make_pair(state, car, cdr);
		return 0;
	}
}

static int read(sm_state *state, sm_reader *reader, sm_word *dst)
{
	int c;
	int sign = 1;
	int i;
	sm_bits num = 0;
#define BUFFER_MAX 1000
	char *buffer;
	char buffer1[BUFFER_MAX];
	char buffer2[BUFFER_MAX];
	buffer = buffer1;
	(void)buffer2;

	skip_whitespace(reader);

	c = next(reader);
	if (c == '#') {
		c = next(reader);
		switch (c) {
		case 't':
			*dst = SM_TRUE;
			return 0;
		case 'f':
			*dst = SM_FALSE;
			return 0;
		default:
			return -1;
		}
	}
	if (isdigit(c) || (c == '-' && (isdigit(peek(reader))))) {
		if (c == '-') {
			sign = -1;
		} else {
			back(reader);
		}

		while (isdigit(c = next(reader))) {
			num = (num * 10) + (c - '0');
		}
		num *= sign;
		if (is_delimiter(c)) {
			back(reader);
			*dst = SM_FIX(num);
			return 0;
		} else {
			return -1;
		}
	} else if (is_initial(c)) {
		i = 0;

		while (is_initial(c) || isdigit(c)) {
			if (c == '.') {
				buffer[i] = '\0';
				buffer = buffer2;
				i = 0;
			} else if (i < BUFFER_MAX - 1) {
				buffer[i++] = c;
			} else {
				return -1;
			}
			c = next(reader);
		}
		if (is_delimiter(c)) {
			buffer[i] = '\0';
			back(reader);
			if (buffer == buffer2) {
				*dst = sm_intern_c(state, buffer1, buffer2);
			} else {
				*dst = sm_intern_c(state, NULL, buffer1);
			}
			return 0;
		} else {
			return -1;
		}
	} else if (c == '"') {
		i = 0;
		buffer = buffer1;
		while ((c = next(reader)) != '"') {
			if (c == '\\') {
				c = next(reader);
				if (c == 'n') {
					c = '\n';
				}
			}
			if (c == EOF) {
				return -1;
			}

			if (i < BUFFER_MAX) {
				buffer[i++] = c;
			} else {
				return -1;
			}
		}
		*dst = sm_make_binary(state, buffer, i);
		return 0;
	} else if (c == '(') {
		return read_pair(state, reader, dst);
	} else if (c == '\'') {
		sm_word val;
		if (read(state, reader, &val) != 0) {
			return -1;
		}
		*dst = sm_make_pair(state, state->quote_symbol, sm_make_pair(state, val, SM_NIL));
		return 0;
	} else if (c == EOF) {
		*dst = SM_EOF;
		return 0;
	} else {
		return -1;
	}
}

void sm_file_reader(sm_state *state, FILE *f, sm_reader *reader)
{
	reader->type = FILE_READER;
	reader->file = f;
}

void sm_string_reader(sm_state *state, const char *str, int size, sm_reader *reader)
{
	reader->type = STRING_READER;
	reader->str.data = str;
	reader->str.size = size;
}

int sm_read(sm_state *state, sm_reader *reader, sm_word *dst)
{
	return read(state, reader, dst);
}
