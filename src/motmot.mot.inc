/* Generated automatically. Do not edit. */
static const char *lib_src =
"(in-ns motmot)"
""
"(def list (fun (a . args) (cons a args)))"
""
"(def defmacro (macro (fun (name formals . body) "
"  (list 'def name "
"        (list 'macro (cons 'fun "
"	                   (cons name "
"			         (cons formals body))))))))"
""
"(defmacro defun (name formals . body)"
"	(list 'def name (cons 'fun (cons name (cons formals body)))))"
""
"\"hi there\""
;
