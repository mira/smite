/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef SM_VM_H_
#define SM_VM_H_

#include "common.h"

#define DEFAULT_STSIZE 256

typedef struct _call_frame {
	sm_word closure;
	int offset;
	sm_word *stack;
} call_frame;

typedef struct _sm_task {
	int n_frames;
	int cap_frames;
	call_frame *frames;
	sm_bits stsize;
	sm_word *stack;
	sm_word *stack_top;
} sm_task;

sm_task *sm_make_task(sm_state *state, sm_bits stsize, sm_word closure,
                      sm_word *args, int n_args);
void sm_free_task(sm_state *state, sm_task *task);
int sm_dispatch(sm_state *state, sm_task *task);
void sm_init_call(sm_state *state, sm_task *task, int n_args);

#endif
