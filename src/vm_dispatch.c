/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "opcode.h"
#include "vm.h"
#include <stdio.h>

int sm_dispatch(sm_state *state, sm_task *task)
{
	call_frame *frame;
	register uint8_t *ip;
	register sm_word proto;
	register sm_word *ststart;

#define STORE_FRAME() frame->offset = ip - SM_RAW(SM_PROTO_CODE(proto));

#define LOAD_FRAME()                                                           \
	do {                                                                   \
		frame = task->frames + (task->n_frames - 1);                   \
		proto = SM_CLOSURE_PROTO(frame->closure);                      \
		ip = SM_RAW(SM_PROTO_CODE(proto)) + frame->offset;             \
		ststart = frame->stack;                                        \
	} while (false)

#define OPDEF(OP, NAME, EFFECT)                                                \
	case OP_##OP:                                                          \
		goto lbl_##OP;
#define NEXT()                                                                 \
	switch (*ip++) {                                                       \
		DEFINE_OPS(OPDEF)                                              \
	}
#define OP_START(OP)                                                           \
	lbl_##OP:                                                              \
	{
#define OP_END(OP)                                                             \
	NEXT();                                                                \
	}

#define PUSH(val) (*task->stack_top++ = val)
#define POP()     (*(--task->stack_top))
#define DROP()    (--task->stack_top)
#define PEEK()    (*(task->stack_top - 1))
#define IMM()     (*ip++)
#define IMM2()    (ip += 2, (uint16_t)((ip[-2] << 8) | ip[-1]))

	LOAD_FRAME();
	NEXT();

	OP_START(NOP)
	OP_END(NOP)

	OP_START(LD_CONST)
	int i = IMM2();
	sm_word val = SM_PROTO_CONST(proto, i);
	PUSH(val);
	OP_END(LD_CONST)

	OP_START(VAR_DEREF)
	sm_word var = POP();
	if (!SM_IS_VAR(var)) {
		return -1;
	} else if (SM_IS_VOID(SM_VAR_VALUE(var))) {
		return -1;
	}
	PUSH(SM_VAR_VALUE(var));
	OP_END(VAR_DEREF)

	OP_START(VAR_SET)
	sm_word val = POP();
	sm_word var = PEEK();
	if (!SM_IS_VAR(var)) {
		return -1;
	}
	SM_VAR_VALUE(var) = val;
	OP_END(VAR_SET)

	OP_START(CALL_N)
	int n_args = IMM() + 1;
	sm_word *args = task->stack_top - n_args;
	if (!SM_IS_OBJ(args[0])) {
		return -1;
	}
	switch (SM_OBJ_TYPE(args[0])) {
	case SM_CLOSURE_TYPE:
		STORE_FRAME();
		sm_init_call(state, task, n_args);
		LOAD_FRAME();
		break;
	case SM_FF_TYPE:
		task->stack_top = args + 1;
		SM_FF_PROTO(args[0])(state, args, n_args - 1);
		break;
	case SM_BINARY_TYPE:
	case SM_TUPLE_TYPE:
		task->stack_top = args + 1;
		if (sm_ref_fun(state, args, n_args - 1) != 0) {
			return -1;
		}
		break;
	default:
		return -1;
	}
	OP_END(CALL_N)

	OP_START(POP)
	DROP();
	OP_END(POP)

	OP_START(STORE_LOCAL)
	ststart[IMM()] = PEEK();
	OP_END(STORE_LOCAL)

	OP_START(LOAD_LOCAL)
	PUSH(ststart[IMM()]);
	OP_END(LOAD_LOCAL)

	OP_START(CLOSURE)
	sm_word closure = sm_make_closure(state, POP(), NULL, 0);
	PUSH(closure);
	OP_END(CLOSURE)

	OP_START(BRANCH)
	sm_word val = POP();
	int offset = IMM2();
	if (!SM_IS_TRUE(val)) {
		ip = SM_RAW(SM_PROTO_CODE(SM_CLOSURE_PROTO(frame->closure))) +
		     offset;
	}
	OP_END(BRANCH)

	OP_START(JUMP)
	ip = SM_RAW(SM_PROTO_CODE(SM_CLOSURE_PROTO(frame->closure))) + IMM2();
	OP_END(JUMP)

	OP_START(BIND_REST)
	int loc = IMM();
	int n = task->stack_top - ststart - loc;
	ststart[loc] = sm_make_list(state, &ststart[loc], n);
	task->stack_top -= n - 1;
	OP_END(BIND_REST)

	OP_START(RET)
	sm_word val = POP();
	ststart[0] = val;
	task->n_frames--;
	if (task->n_frames <= 0) {
		return 0;
	}
	task->stack_top = frame->stack + 1;
	LOAD_FRAME();
	OP_END(RET)

	return 0;
}
