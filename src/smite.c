/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <smite/smite.h>
#include <stdio.h>


static int repl(sm_state *state)
{
	sm_reader reader;
	sm_file_reader(state, stdin, &reader);
	while (true) {
		sm_word ns = SM_NS_NAME(sm_current_ns(state));
		sm_word ns_name = SM_SYMBOL_NAME(ns);
		printf("%.*s > ", (int)SM_OBJ_SIZE(ns_name), SM_RAW(ns_name));
		sm_word code;
		if (sm_read(state, &reader, &code) != 0) {
			printf("reader error\n");
			continue;
		}
		sm_word result;
		if (sm_eval(state, code, SM_NIL, &result) != 0) {
			printf("error\n");
			continue;
		}
		if (!SM_IS_VOID(result)) {
			sm_print_value(result);
			printf("\n");
		}
	}
	return 0;
}

int main(int argc, char **argv)
{
	sm_state *state = sm_make_state();
	sm_load_motmot(state);
	repl(state);
	sm_free_state(state);
	return 0;
}
