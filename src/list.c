/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "common.h"

sm_word sm_make_list(sm_state *state, sm_word *data, sm_bits size)
{
	sm_word list = SM_NIL;
	for (int i = size - 1; i >= 0; i--) {
		list = sm_make_pair(state, data[i], list);
	}
	return list;
}
