/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "vm.h"
#include "state.h"
#include <stdio.h>

void sm_init_call(sm_state *state, sm_task *task, int n_args)
{
	call_frame *frame = task->frames + (task->n_frames++);
	frame->stack = task->stack_top - n_args;
	frame->closure = frame->stack[0];
	frame->offset = 0;
}

sm_task *sm_make_task(sm_state *state, sm_bits stsize, sm_word closure,
                      sm_word *args, int n_args)
{
	if (stsize == 0) {
		stsize = DEFAULT_STSIZE;
	}
	sm_task *task = sm_malloc(state, sizeof(*task));
	task->cap_frames = 16;
	task->frames = sm_malloc(state, sizeof(*task->frames) * 16);
	task->n_frames = 0;
	task->stsize = stsize;
	task->stack = sm_malloc(state, sizeof(*task->stack) * stsize);
	task->stack[0] = closure;
	task->stack_top = task->stack + n_args + 1;
	memcpy(task->stack + 1, args, sizeof(*args) * n_args);
	sm_init_call(state, task, n_args + 1);
	return task;
}

void sm_free_task(sm_state *state, sm_task *task)
{
	sm_free(state, task->frames);
	sm_free(state, task->stack);
	sm_free(state, task);
}

int sm_eval(sm_state *state, sm_word expr, sm_word ns, sm_word *dst)
{
	sm_word closure;
	if (sm_compile(state, expr, ns, &closure) != 0) {
		printf("no compiler\n");
		return -1;
	}
	sm_task *task = sm_make_task(state, 0, closure, NULL, 0);
	int result = sm_dispatch(state, task);
	if (result == 0 && dst != NULL) {
		*dst = task->stack[0];
	} else {
		printf("no runtime\n");
	}
	sm_free_task(state, task);
	return result;
}

int sm_load_file(sm_state *state, const char *name, sm_word *dst)
{
	sm_word ns = state->ns;
	FILE *f = fopen(name, "r");
	if (f == NULL) {
		return -1;
	}
	sm_reader reader;
	sm_word val;
	int result = 0;
	sm_file_reader(state, f, &reader);
	sm_read(state, &reader, &val);
	while (val != SM_EOF) {
		result = sm_eval(state, val, SM_NIL, dst);
		if (result != 0) {
			break;
		}
		result = sm_read(state, &reader, &val);
		if (result != 0) {
			break;
		}
	}
	state->ns = ns;
	return result;
}

int sm_load_string(sm_state *state, const char *str, sm_word *dst)
{
	sm_reader reader;
	sm_word val;
	sm_word ns = state->ns;
	int result = 0;
	sm_string_reader(state, str, strlen(str), &reader);
	sm_read(state, &reader, &val);
	while (val != SM_EOF) {
		result = sm_eval(state, val, SM_NIL, dst);
		if (result != 0) {
			break;
		}
		result = sm_read(state, &reader, &val);
		if (result != 0) {
			break;
		}
	}
	state->ns = ns;
	return result;
}
