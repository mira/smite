/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef SM_COMMON_H_
#define SM_COMMON_H_

#include <smite/smite.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#define SM_TABLE_DEFAULT_SIZE 16
#define SM_TABLE_MAX_LOAD     80
#define SM_TABLE_GROW_FACTOR  2

uint32_t sm_hash_bits(sm_word hash);
uint32_t sm_hash_raw(const char *data, size_t size);
uint32_t sm_hash_symbol_raw(const char *ns, size_t ns_size, const char *name,
                            size_t name_size);
uint32_t sm_hash_symbol(sm_word obj);
uint32_t sm_hash_obj(sm_word obj);
uint32_t sm_hash_value(sm_word val);

void sm_resize_table(sm_state *state, sm_word table, sm_word cap);
int sm_ref_fun(sm_state *state, sm_word *args, int n_args);

#endif
