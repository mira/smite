/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef SM_STATE_H_
#define SM_STATE_H_

#include "common.h"

#define SM_SLOTS 64

typedef struct _sm_state {
	sm_word symbols;
	sm_word namespaces;
	sm_word ns;
	int allocated_words;
	sm_word *aheap_base;
	sm_word *aheap_top;
	sm_word *aheap;
	sm_word aheap_list;
	sm_word def_symbol;
	sm_word in_ns_symbol;
	sm_word let_symbol;
	sm_word fun_symbol;
	sm_word quote_symbol;
	sm_word var_symbol;
	sm_word require_symbol;
	sm_word if_symbol;
} sm_state;

sm_word sm_aalloc(sm_state *state, sm_bits size);
sm_word sm_palloc(sm_state *state, sm_bits size);
void *sm_malloc(sm_state *state, sm_bits size);
void sm_free(sm_state *state, void *ptr);

#endif
