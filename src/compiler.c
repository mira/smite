/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "compiler-internal.h"
#include "opcode.h"
#include "vm.h"

static int compile_expr(sm_compiler *compiler, c_scope *scope, sm_word expr);

static int add_local(c_scope *scope, sm_word name)
{
	c_var *var = &scope->locals[scope->n_locals++];
	var->loc = scope->stack_depth++;
	if (scope->stack_depth > scope->max_stack_depth) {
		scope->max_stack_depth = scope->stack_depth;
	}
	var->name = name;
	return var->loc;
}

static void init_scope(c_scope *scope, c_scope *parent, sm_word name)
{
	scope->parent = parent;
	scope->n_locals = 0;
	scope->n_upvals = 0;
	scope->n_consts = 0;
	scope->code_size = 0;
	scope->stack_depth = 0;
	scope->max_stack_depth = 0;
	scope->arity = 0;
	add_local(scope, name);
}

static sm_word end_scope(sm_compiler *compiler, c_scope *scope)
{
	// for (int i = 0; i < scope->code_size; i++) {
	// 	printf("%i: %i\n", i, scope->code[i]);
	// }
	sm_word code =
	    sm_make_binary(compiler->state, scope->code, scope->code_size);
	sm_word proto = sm_make_proto(compiler->state, SM_NIL, code,
	                              scope->consts, scope->n_consts);
	return proto;
}

static void emit_op(c_scope *scope, int op)
{
	scope->code[scope->code_size++] = op;
	scope->stack_depth += sm_op_effect[op];
}

static void emit_byte(c_scope *scope, int byte)
{
	scope->code_size++;
	scope->code[scope->code_size - 1] = byte;
}

static void emit_short(c_scope *scope, int data)
{
	emit_byte(scope, (data >> 8) & 0xff);
	emit_byte(scope, data & 0xff);
}

static int emit_br(c_scope *scope)
{
	emit_op(scope, OP_BRANCH);
	emit_short(scope, 0);
	return scope->code_size - 2;
}

static int emit_jmp(c_scope *scope)
{
	emit_op(scope, OP_JUMP);
	emit_short(scope, 0);
	return scope->code_size - 2;
}

static void patch_jump(c_scope *scope, int i)
{
	int dst = scope->code_size;
	scope->code[i] = (dst >> 8) & 0xff;
	scope->code[i + 1] = dst & 0xff;
}

static int compile_quote(sm_compiler *compiler, c_scope *scope, sm_word expr)
{
	/* TODO: copy mutable objects */
	scope->consts[scope->n_consts++] = expr;
	emit_op(scope, OP_LD_CONST);
	emit_short(scope, scope->n_consts - 1);
	return 0;
}

static sm_word get_var(sm_compiler *compiler, sm_word expr)
{
	if (sm_symbol_has_ns(expr)) {
		sm_word var = sm_var(compiler->state, expr);
		if (!SM_IS_VAR(var)) {
			return SM_VOID;
		}
		return var;
	}

	sm_word mappings = SM_NS_MAPPINGS(compiler->ns);
	sm_word var = sm_table_get(compiler->state, mappings, expr);
	if (!SM_IS_VOID(var)) {
		return var;
	}

	sm_word full_name = sm_intern(
	    compiler->state, sm_make_binary(compiler->state, "motmot", 6),
	    SM_SYMBOL_NAME(expr));
	var = sm_var(compiler->state, full_name);
	if (!SM_IS_VOID(var)) {
		return var;
	}
	return SM_VOID;
}

static int compile_var(sm_compiler *compiler, c_scope *scope, sm_word expr)
{
	sm_word var = get_var(compiler, expr);
	if (SM_IS_VOID(var)) {
		return -1;
	}
	return compile_quote(compiler, scope, var);
}

static int compile_def(sm_compiler *compiler, c_scope *scope, sm_word expr)
{
	sm_word name = SM_CAR(expr);
	sm_word val = SM_CAR(SM_CDR(expr));
	sm_word ns = SM_NS_NAME(compiler->ns);
	sm_word full_name = sm_intern(compiler->state, SM_SYMBOL_NAME(ns),
	                              SM_SYMBOL_NAME(name));
	sm_word var = sm_var(compiler->state, full_name);
	sm_ns_map(compiler->state, compiler->ns, name, var);
	compile_quote(compiler, scope, var);
	compile_expr(compiler, scope, val);
	emit_op(scope, OP_VAR_SET);
	return 0;
}

static int compile_in_ns(sm_compiler *compiler, c_scope *scope, sm_word expr)
{
	sm_word name = SM_CAR(expr);
	sm_word ns = sm_get_ns(compiler->state, name);
	compiler->ns = ns;
	compile_quote(compiler, scope, SM_VOID);
	return 0;
}

static int compile_let(sm_compiler *compiler, c_scope *scope, sm_word expr)
{
	sm_word ns = compiler->ns;
	int n_locals = scope->n_locals;
	int stack_depth = scope->stack_depth;
	if (!SM_IS_PAIR(expr)) {
		return -1;
	}
	sm_word next = SM_CAR(expr);
	while (!SM_IS_NIL(next)) {
		sm_word name = SM_CAR(next);
		next = SM_CDR(next);
		sm_word val = SM_CAR(next);
		next = SM_CDR(next);
		int loc = add_local(scope, name);
		compile_expr(compiler, scope, val);
		emit_op(scope, OP_STORE_LOCAL);
		emit_byte(scope, loc);
	}
	next = SM_CDR(expr);
	compile_expr(compiler, scope, SM_CAR(next));
	next = SM_CDR(next);
	while (!SM_IS_NIL(next)) {
		emit_op(scope, OP_POP);
		compile_expr(compiler, scope, SM_CAR(next));
		next = SM_CDR(next);
	}
	emit_op(scope, OP_STORE_LOCAL);
	emit_byte(scope, stack_depth);
	while (scope->stack_depth > stack_depth) {
		emit_op(scope, OP_POP);
	}
	scope->n_locals = n_locals;
	compiler->ns = ns;
	return 0;
}

static int compile_fun(sm_compiler *compiler, c_scope *scope, sm_word expr)
{
	c_scope new_scope;
	sm_word ns = compiler->ns;
	sm_word fun_name = SM_NIL;
	sm_word params = SM_CAR(expr);
	if (SM_IS_SYMBOL(params)) {
		expr = SM_CDR(expr);
		fun_name = params;
		params = SM_CAR(expr);
	}
	init_scope(&new_scope, scope, fun_name);
	expr = SM_CDR(expr);
	while (!SM_IS_NIL(params)) {
		if (SM_IS_PAIR(params)) {
			sm_word name = SM_CAR(params);
			add_local(&new_scope, name);
			params = SM_CDR(params);
		} else if (SM_IS_SYMBOL(params)) {
			int loc = add_local(&new_scope, params);
			emit_op(&new_scope, OP_BIND_REST);
			emit_byte(&new_scope, loc);
			break;
		}
	}
	/* compile body */
	while (!SM_IS_NIL(expr)) {
		compile_expr(compiler, &new_scope, SM_CAR(expr));
		expr = SM_CDR(expr);
	}
	emit_op(&new_scope, OP_RET);
	compile_quote(compiler, scope, end_scope(compiler, &new_scope));
	emit_op(scope, OP_CLOSURE);
	compiler->ns = ns;
	return 0;
}

static int compile_if(sm_compiler *compiler, c_scope *scope, sm_word list)
{
	if (SM_IS_NIL(list)) {
		compile_quote(compiler, scope, SM_VOID);
		return 0;
	}
	sm_word cond = SM_CAR(list);
	compile_expr(compiler, scope, cond);
	list = SM_CDR(list);
	if (SM_IS_NIL(list)) {
		return 0;
	}
	int else_jmp = emit_br(scope);
	sm_word expr = SM_CAR(list);
	compile_expr(compiler, scope, expr);
	int jmp = emit_jmp(scope);
	patch_jump(scope, else_jmp);
	int result = compile_if(compiler, scope, SM_CDR(list));
	patch_jump(scope, jmp);
	return result;
}

static int compile_macro(sm_compiler *compiler, c_scope *scope, sm_word expr)
{
	sm_word symbol = SM_CAR(expr);
	sm_word var = get_var(compiler, symbol);
	if (SM_IS_VOID(var)) {
		return 1;
	}
	sm_word val = SM_VAR_VALUE(var);
	if (!SM_IS_MACRO(val)) {
		return 1;
	}
	int n_args = 0;
	sm_word args[64];
	expr = SM_CDR(expr);
	while (SM_IS_PAIR(expr)) {
		args[n_args++] = SM_CAR(expr);
		expr = SM_CDR(expr);
	}
	sm_task *task = sm_make_task(compiler->state, 0, SM_MACRO_CLOSURE(val),
	                             args, n_args);
	int result = sm_dispatch(compiler->state, task);
	if (result != 0) {
		return result;
	}
	expr = task->stack[0];
	sm_free_task(compiler->state, task);
	return compile_expr(compiler, scope, expr);
}

static int compile_pair(sm_compiler *compiler, c_scope *scope, sm_word expr)
{
	int i = -1;
	if (SM_IS_SYMBOL(SM_CAR(expr))) {
		sm_word symbol = SM_CAR(expr);
		if (symbol == compiler->state->def_symbol) {
			return compile_def(compiler, scope, SM_CDR(expr));
		} else if (symbol == compiler->state->in_ns_symbol) {
			return compile_in_ns(compiler, scope, SM_CDR(expr));
		} else if (symbol == compiler->state->let_symbol) {
			return compile_let(compiler, scope, SM_CDR(expr));
		} else if (symbol == compiler->state->fun_symbol) {
			return compile_fun(compiler, scope, SM_CDR(expr));
		} else if (symbol == compiler->state->quote_symbol) {
			if (!SM_IS_PAIR(SM_CDR(expr))) {
				return -1;
			}
			return compile_quote(compiler, scope,
			                     SM_CAR(SM_CDR(expr)));
		} else if (symbol == compiler->state->var_symbol) {
			if (!SM_IS_PAIR(SM_CDR(expr))) {
				return -1;
			}
			return compile_var(compiler, scope,
			                   SM_CAR(SM_CDR(expr)));
		} else if (symbol == compiler->state->if_symbol) {
			return compile_if(compiler, scope, SM_CDR(expr));
		} else {
			int res = compile_macro(compiler, scope, expr);
			if (res != 1) {
				return res;
			}
		}
	}
	while (SM_IS_PAIR(expr)) {
		int res = compile_expr(compiler, scope, SM_CAR(expr));
		if (res != 0) {
			return res;
		}
		expr = SM_CDR(expr);
		i++;
	}
	if (!SM_IS_NIL(expr)) {
		return -1;
	}
	emit_op(scope, OP_CALL_N);
	emit_byte(scope, i);
	scope->stack_depth -= i;
	return 0;
}

static int resolve_local(c_scope *scope, sm_word name)
{
	for (int i = 0; i < scope->n_locals; i++) {
		if (scope->locals[i].name == name) {
			return scope->locals[i].loc;
		}
	}
	return -1;
}

static int add_upval(c_scope *scope, int loc, bool is_local)
{
	for (int i = 0; i < scope->n_upvals; i++) {
		if (scope->upvals[i].is_local == is_local &&
		    scope->upvals[i].loc == loc) {
			return i;
		}
	}
	c_upval *upval = &scope->upvals[scope->n_upvals];
	upval->is_local = is_local;
	upval->loc = loc;
	return scope->n_upvals++;
}

static int resolve_upval(c_scope *scope, sm_word name)
{
	if (scope->parent == NULL) {
		/* toplevel can't have upvalues */
		return -1;
	}

	int local = resolve_local(scope->parent, name);
	if (local != -1) {
		return add_upval(scope, local, true);
	}

	int upval = resolve_upval(scope->parent, name);
	if (upval != -1) {
		return add_upval(scope, upval, false);
	}

	return -1;
}

static int lookup_symbol(sm_compiler *compiler, c_scope *scope, sm_word expr)
{
	int local = resolve_local(scope, expr);
	if (local != -1) {
		emit_op(scope, OP_LOAD_LOCAL);
		emit_byte(scope, local);
		return 0;
	}

	int upval = resolve_upval(scope, expr);
	if (upval != -1) {

		return 0;
	}

	sm_word mappings = SM_NS_MAPPINGS(compiler->ns);
	sm_word var = sm_table_get(compiler->state, mappings, expr);
	if (!SM_IS_VOID(var)) {
		if (compile_quote(compiler, scope, var) != 0) {
			return -1;
		}
		emit_op(scope, OP_VAR_DEREF);
		return 0;
	}
	sm_word full_name = sm_intern(
	    compiler->state, sm_make_binary(compiler->state, "motmot", 6),
	    SM_SYMBOL_NAME(expr));
	var = sm_var(compiler->state, full_name);
	if (!SM_IS_VOID(var)) {
		if (compile_quote(compiler, scope, var) != 0) {
			return -1;
		}
		emit_op(scope, OP_VAR_DEREF);
		return 0;
	}
	return -1;
}

static int compile_symbol(sm_compiler *compiler, c_scope *scope, sm_word expr)
{
	if (sm_symbol_has_ns(expr)) {
		sm_word var = sm_var(compiler->state, expr);
		if (!SM_IS_VAR(var)) {
			return -1;
		} else if (compile_quote(compiler, scope, var) != 0) {
			return -1;
		}
		emit_op(scope, OP_VAR_DEREF);
		return 0;
	} else {
		return lookup_symbol(compiler, scope, expr);
	}
}

static int compile_obj(sm_compiler *compiler, c_scope *scope, sm_word expr)
{
	switch (SM_OBJ_TYPE(expr)) {
	case SM_PAIR_TYPE:
		return compile_pair(compiler, scope, expr);
	case SM_SYMBOL_TYPE:
		return compile_symbol(compiler, scope, expr);
	case SM_TUPLE_TYPE:
	case SM_BINARY_TYPE:
	case SM_TABLE_TYPE:
	case SM_VAR_TYPE:
		return compile_quote(compiler, scope, expr);
	}
	return -1;
}

static int compile_expr(sm_compiler *compiler, c_scope *scope, sm_word expr)
{
	if (SM_IS_OBJ(expr)) {
		return compile_obj(compiler, scope, expr);
	}
	compile_quote(compiler, scope, expr);
	return 0;
}

int sm_compile(sm_state *state, sm_word expr, sm_word ns, sm_word *dst)
{
	if (ns == SM_NIL) {
		ns = state->ns;
	}
	sm_compiler compiler = {state, ns};
	c_scope toplevel;
	init_scope(&toplevel, NULL, SM_NIL);
	if (compile_expr(&compiler, &toplevel, expr) != 0) {
		return -1;
	}
	emit_op(&toplevel, OP_RET);
	if (compiler.ns != ns) {
		state->ns = compiler.ns;
	}
	sm_word proto = end_scope(&compiler, &toplevel);
	*dst = sm_make_closure(state, proto, NULL, 0);
	return 0;
}
