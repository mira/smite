/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "state.h"
#include <stdlib.h>

sm_state *sm_make_state()
{
	sm_state *state = malloc(sizeof(*state));
	state->allocated_words = 0;
	state->aheap_base =
	    malloc(sizeof(*state->aheap_base) * SM_DEFAULT_AHEAP_SIZE);
	state->aheap_top = state->aheap_base + SM_DEFAULT_AHEAP_SIZE;
	state->aheap = state->aheap_base;
	state->aheap_list = SM_NIL;
	state->symbols = sm_make_empty_table(state, 0);
	state->namespaces = sm_make_empty_table(state, 0);
	state->ns = sm_make_ns(state, sm_intern_c(state, NULL, "user"));
	state->def_symbol = sm_intern_c(state, NULL, "def");
	state->in_ns_symbol = sm_intern_c(state, NULL, "in-ns");
	state->let_symbol = sm_intern_c(state, NULL, "let");
	state->fun_symbol = sm_intern_c(state, NULL, "fun");
	state->quote_symbol = sm_intern_c(state, NULL, "quote");
	state->var_symbol = sm_intern_c(state, NULL, "var");
	state->require_symbol = sm_intern_c(state, NULL, "require");
	state->if_symbol = sm_intern_c(state, NULL, "if");
	return state;
}

void sm_free_state(sm_state *state)
{
	free(state->aheap_base);
	sm_word e = state->aheap_list;
	sm_word next;
	while (e != SM_NIL) {
		next = *(sm_word *)e;
		free((sm_word *)e);
		e = next;
	}
	free(state);
}

sm_word sm_aalloc(sm_state *state, sm_word size)
{
	if (state->aheap + size >= state->aheap_top) {
		/* TODO: invoke gc */
		return 0;
	}
	sm_word *ptr = state->aheap;
	state->aheap += size;
	state->allocated_words += size;
	return (sm_word)ptr;
}

sm_word sm_palloc(sm_state *state, sm_bits size)
{
	sm_word *ptr = malloc(sizeof(sm_word) * (size + 2));
	*ptr = state->aheap_list;
	*(ptr + 1) = SM_NIL;
	state->aheap_list = (sm_word)ptr;
	state->allocated_words += size;
	return (sm_word)(ptr + 2);
}

void *sm_malloc(sm_state *state, sm_bits size)
{
	return malloc(size);
}

void sm_free(sm_state *state, void *ptr)
{
	free(ptr);
}

int sm_allocated_words(sm_state *state)
{
	return state->allocated_words;
}

int sm_allocated_bytes(sm_state *state)
{
	return state->allocated_words * sizeof(sm_word);
}

sm_word sm_symbol_table(sm_state *state)
{
	return state->symbols;
}

sm_word sm_get_ns(sm_state *state, sm_word name)
{
	sm_word ns = sm_table_get(state, state->namespaces, name);
	if (SM_IS_VOID(ns)) {
		return sm_make_ns(state, name);
	}
	return ns;
}

sm_word sm_ns_table(sm_state *state)
{
	return state->namespaces;
}

sm_word sm_current_ns(sm_state *state)
{
	return state->ns;
}

sm_word sm_var(sm_state *state, sm_word symbol)
{
	sm_word var = sm_table_get(state, state->symbols, symbol);
	if (SM_IS_VOID(var)) {
		var = sm_make_var(state, symbol);
		sm_table_set(state, state->symbols, symbol, var);
	}
	return var;
}

sm_word sm_def(sm_state *state, sm_word symbol, sm_word val)
{
	sm_word var = sm_var(state, symbol);
	SM_VAR_VALUE(var) = val;
	return var;
}
