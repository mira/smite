/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "common.h"
#include "motmot.mot.inc"

static int add_fun(sm_state *state, sm_word *args, int n_args)
{
	if (n_args != 2) {
		return -1;
	}
	sm_word result = SM_UNFIX(args[1]) + SM_UNFIX(args[2]);
	args[0] = SM_FIX(result);
	return 0;
}

static int sub_fun(sm_state *state, sm_word *args, int n_args)
{
	if (n_args < 2) {
		return -1;
	}
	sm_bits result;
	result = SM_UNFIX(args[1]);
	for (int i = 2; i <= n_args; i++) {
		if (!SM_IS_FIXNUM(args[i])) {
			return -1;
		}
		result -= SM_UNFIX(args[i]);
	}
	args[0] = SM_FIX(result);
	return 0;
}

static int println_fun(sm_state *state, sm_word *args, int n_args)
{
	for (int i = 0; i < n_args - 1; i++) {
		sm_print_value(args[i]);
		printf(" ");
	}
	sm_print_value(args[n_args]);
	printf("\n");
	args[0] = SM_NIL;
	return 0;
}

static int macro_fun(sm_state *state, sm_word *args, int n_args)
{
	if (n_args != 1) {
		return -1;
	}
	args[0] = sm_make_macro(state, args[1]);
	return 0;
}

static int cons_fun(sm_state *state, sm_word *args, int n_args)
{
	if (n_args != 2) {
		return -1;
	}
	args[0] = sm_make_pair(state, args[1], args[2]);
	return 0;
}

static int le_fun(sm_state *state, sm_word *args, int n_args)
{
	if (n_args != 2) {
		return -1;
	}
	if (SM_UNFIX(args[1]) <= SM_UNFIX(args[2])) {
		args[0] = SM_TRUE;
	} else {
		args[0] = SM_FALSE;
	}
	return 0;
}

int sm_load_motmot(sm_state *state)
{
#define FUN(ns, name, fn) sm_def(state, sm_intern_c(state, ns, name), sm_make_ff(state, fn, NULL, 0));
	FUN("motmot", "+", add_fun);
	FUN("motmot", "-", sub_fun);
	FUN("motmot", "println!", println_fun);
	FUN("motmot", "macro", macro_fun);
	FUN("motmot", "cons", cons_fun);
	FUN("motmot", "<=", le_fun);
	sm_word val;
	int result = sm_load_string(state, lib_src, &val);
	if (result != 0) {
		return result;
	}
#undef FUN
}
