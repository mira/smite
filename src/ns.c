/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "state.h"

sm_word sm_make_ns(sm_state *state, sm_word name)
{
	sm_word ns = sm_aalloc(state, SM_NS_SIZE + 1);
	SM_OFFSET(ns, 0) = SM_NS_TYPE | SM_NS_SIZE;
	SM_OFFSET(ns, 1) = name;
	SM_OFFSET(ns, 2) = sm_make_empty_table(state, 0);
	SM_OFFSET(ns, 3) = sm_make_empty_table(state, 0);
	sm_table_set(state, state->namespaces, name, ns);
	return ns;
}

void sm_ns_alias(sm_state *state, sm_word ns, sm_word t, sm_word alias)
{
	if (!SM_IS_NS(ns)) {
		return;
	}
	sm_table_set(state, SM_NS_ALIAS(ns), SM_SYMBOL_NAME(t),
	             SM_SYMBOL_NAME(alias));
}

void sm_ns_map(sm_state *state, sm_word ns, sm_word name, sm_word var)
{
	if (!SM_IS_NS(ns)) {
		return;
	}
	sm_table_set(state, SM_NS_MAPPINGS(ns), name, var);
}
