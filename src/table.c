/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "state.h"

#ifdef SM_64
uint32_t sm_hash_bits(sm_word hash)
{
	hash = ~hash + (hash << 18); // hash = (hash << 18) - hash - 1;
	hash = hash ^ (hash >> 31);
	hash = hash * 21; // hash = (hash + (hash << 2)) + (hash << 4);
	hash = hash ^ (hash >> 11);
	hash = hash + (hash << 6);
	hash = hash ^ (hash >> 22);
	return (uint32_t)(hash & 0x3fffffff);
}
#endif

uint32_t sm_hash_raw(const char *data, sm_bits size)
{
	/* FNV-1a hash. See: http://www.isthe.com/chongo/tech/comp/fnv/ */
	uint32_t hash = 2166136261u;
	for (uint32_t i = 0; i < size; i++) {
		hash ^= data[i];
		hash *= 16777619;
	}
	return hash;
}

uint32_t sm_hash_symbol_raw(const char *ns, sm_bits ns_size, const char *name,
                            sm_bits name_size)
{
	sm_word ns_hash = 0;
	sm_word name_hash = sm_hash_raw(name, name_size);
	if (ns != NULL) {
		ns_hash = sm_hash_raw(ns, ns_size);
	}
	return sm_hash_bits(ns_hash << 32 | name_hash);
}

uint32_t sm_hash_symbol(sm_word obj)
{
	sm_word ns_hash = 0;
	sm_word name_hash = sm_hash_binary(SM_SYMBOL_NAME(obj));
	if (!SM_IS_NIL(SM_SYMBOL_NS(obj))) {
		ns_hash = sm_hash_binary(SM_SYMBOL_NS(obj));
	}
	return sm_hash_bits(ns_hash << 32 | name_hash);
}

uint32_t sm_hash_obj(sm_word obj)
{
	switch (SM_OBJ_TYPE(obj)) {
	case SM_BINARY_TYPE:
		return sm_hash_binary(obj);
	case SM_SYMBOL_TYPE:
		return sm_hash_symbol(obj);
	default:
		/* TODO: freak out */
		return 0;
	}
}

uint32_t sm_hash_value(sm_word val)
{
	if (SM_IS_OBJ(val)) {
		return sm_hash_obj(val);
	}
	return sm_hash_bits(val);
}

sm_word sm_make_empty_table(sm_state *state, sm_word size)
{
	if (size == 0) {
		size = SM_TABLE_DEFAULT_SIZE;
	}
	sm_word table = sm_aalloc(state, SM_TABLE_SIZE + 1);
	SM_OFFSET(table, 0) = SM_TABLE_TYPE | SM_TABLE_SIZE;
	SM_OFFSET(table, 1) = SM_FIX(size);
	SM_OFFSET(table, 2) = SM_FIX(0);
	SM_OFFSET(table, 3) = sm_make_empty_tuple(state, size);
	return table;
}

static int find_entry(sm_state *state, sm_word table, sm_word key,
                      sm_word *result, bool create)
{
	sm_word size = SM_TABLE_CAP(table);
	if (size == 0) {
		return -1;
	}
	uint32_t hash = sm_hash_value(key);
	uint32_t start_i = hash % size;
	uint32_t i = start_i;

	sm_word empty = SM_NIL;
	do {
		sm_word entry = SM_TABLE_ENTRY(table, i);
		if (SM_IS_VOID(entry)) {
			/* key is not in table */
			if (!SM_IS_NIL(empty)) {
				*result = empty;
			} else if (create) {
				/* create a new key */
				sm_word pair =
				    sm_make_pair(state, SM_VOID, SM_VOID);
				SM_TABLE_ENTRY(table, i) = pair;
				*result = pair;
			}
			return 1;
		} else if (SM_IS_VOID(SM_CAR(entry))) {
			if (SM_IS_NIL(empty)) {
				empty = entry;
			}
		} else if (sm_is_eq(SM_CAR(entry), key)) {
			*result = entry;
			return 0;
		}
		i = (i + 1) % size;
	} while (i != start_i);
	/* either the table is full or there's an empty slot */
	if (empty == -1) {
		/* table is full */
		return -1;
	} else {
		*result = empty;
		return 1;
	}
}

static int table_insert(sm_state *state, sm_word table, sm_word key,
                        sm_word val)
{
	sm_word entry;
	if (find_entry(state, table, key, &entry, true) == 0) {
		SM_CDR(entry) = val;
		return 0;
	} else {
		SM_CAR(entry) = key;
		SM_CDR(entry) = val;
		return 1;
	}
}

void sm_resize_table(sm_state *state, sm_word table, sm_word cap)
{
	sm_word entries = SM_TABLE_ENTRIES(table);
	sm_word old_cap = SM_TABLE_CAP(table);

	SM_TABLE_ENTRIES(table) = sm_make_empty_tuple(state, cap);

	/* re-add existing entries */
	for (sm_word i = 0; i < old_cap; i++) {
		sm_word entry = SM_OFFSET(entries, i + 1);

		if (!SM_IS_VOID(entry)) {
			table_insert(state, table, SM_CAR(entry),
			             SM_CDR(entry));
		}
	}
}

sm_word sm_table_get(sm_state *state, sm_word table, sm_word key)
{
	if (!SM_IS_TABLE(table)) {
		return SM_NIL;
	}
	sm_word entry;
	if (find_entry(state, table, key, &entry, false) == 0) {
		return SM_CDR(entry);
	}
	return SM_VOID;
}

void sm_table_set(sm_state *state, sm_word table, sm_word key, sm_word val)
{
	if (!SM_IS_TABLE(table)) {
		return;
	}
	if (SM_TABLE_USED(table) + 1 >
	    SM_TABLE_CAP(table) * SM_TABLE_MAX_LOAD / 100) {
		sm_word cap = SM_TABLE_CAP(table) * SM_TABLE_GROW_FACTOR;

		sm_resize_table(state, table, cap);
	}

	if (table_insert(state, table, key, val) == 1) {
		SM_TABLE_USED(table) =
		    SM_FIX(SM_UNFIX(SM_TABLE_USED(table)) + 1);
	}
}
