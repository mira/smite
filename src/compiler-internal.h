/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef SM_COMPILER_INTERNAL_H_
#define SM_COMPILER_INTERNAL_H_

#include "state.h"

#define MAX_LOCALS 64
#define MAX_UPVALS 64
#define MAX_CONSTS 64
#define MAX_CODE   64

typedef struct _sm_compiler {
	sm_state *state;
	sm_word ns;
} sm_compiler;

typedef struct _c_var {
	int loc;
	sm_word name;
} c_var;

typedef struct _c_upval {
	int is_local;
	int loc;
} c_upval;

typedef struct _c_scope {
	struct _c_scope *parent;
	int depth;
	int arity;
	int max_stack_depth;
	int stack_depth;
	int n_locals;
	c_var locals[MAX_LOCALS];
	int n_upvals;
	c_upval upvals[MAX_UPVALS];
	int n_consts;
	sm_word consts[MAX_CONSTS];
	int code_size;
	char code[MAX_CODE];
} c_scope;

#endif
