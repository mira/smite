

default:
	@$(MAKE) -C src

format:
	clang-format -i -style=file src/*.c src/*.h include/smite/*.h

clean:
	@$(MAKE) -C src clean